# OpenVPN Client generator

OpenVPN Client generator is a set of scripts which allows to define new users for an OpenVPN Server using Easy-RSA 3.

## Usage

The users (and certificates, configuration file and so on) must follow these steps:

1. Run ```./generate-base-conf.sh```, answer to questions
2. Run ```./generate-client.sh``` for each user that you want to add
3. Optionally (if needed) run ```./revoke-client.sh``` for revoke a user access to the OpenVPN server (this will also generate the crl list and add to server configuration).

## Known Issues

* These scripts are feasible with a single server scenario, in case of multiple remote server you have to run these scripts on the same server where the is the Easy-RSA PKI.
  * In case of ```revoke-client.sh``` the crl.pem must be spreaded to all servers.
* The ```generate-client.sh``` could create one user at time, this will be fixed in future release.

## Future features

* Windows Support
