#! /usr/bin/env sh

set -e

cur_dir=$(pwd)
. config.env

cleanup (){
    echo "Revoking, nothing to cleanup"
}

restart_ovpn(){
    if $(uname | grep -e 'BSD'); then
        $_ex "service openvpn restart"
    else
        $_ex "systemctl restart openvpn@.service"
    fi
}

check_ovpn_conf() {
    if ! $(grep "crl-verify" $1); then
        $_ex "echo crl-verify $EASYRSA/pki/crl.pem" >> $1
        echo "Configuration updated restarting server"
        restart_ovpn
    else
        echo "crl-verify is in configuration, please be sure that use the correct crl"
    fi
}

if [ "$1" = "" ]; then
        _usage
fi

cd $EASYRSA

echo "Revoking client $1"
$_ex "env EASYRSA=$EASYRSA $EASYRSA_EXEC revoke $1"
$_ex "mkdir -p $EASYRSA_PKI/{revoked,renewed}/{reqs_by_serial, private_by_serial, certs_by_serial}"

echo "Generating certificate revocation list"
$_ex "env EASYRSA=$EASYRSA $EASYRSA_EXEC revoke $1"

read -p "Do you want to check the openvpn configuration?[y/N] " check_conf
if check_answer $check_conf; then
    read -p "Please insert the server configuration path and name: " srv_conf
    check_ovpn_conf $srv_conf
else 
    echo "Certificate for $1 revoked"
fi     

cd $cur_dir

echo "Removing configuration"
$_ex "rm $1.ovpn"
