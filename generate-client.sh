#! /usr/bin/env sh

set -e

cleanup(){
	$_ex "rm $1.ovpn rm $1.ovpn.bak 2> /dev/null"
}

cur_dir=$(pwd)
config_file="config.env"

if [ ! -e $config_file ]; then
	echo "Config file not found, run generate-base-conf.sh first"
	exit 1
fi

. $config_file

if [ "$1" = "" ]; then
        _usage
fi

read -p "Do you want to generate the certificate with the password?[Y/n] " password 

cd $EASYRSA_PKI

if check_answer $password; then
	$_ex "env EASYRSA=$EASYRSA $EASYRSA_EXEC build-client-full $1"
else
	$_ex "env EASYRSA=$EASYRSA $EASYRSA_EXEC build-client-full $1 nopass"
fi

if [ $? != 0 ]; then
	$_ex "rm $EASYRSA_PKI/private/$1.key"
	$_ex "rm $EASYRSA_PKI/reqs/$1.req"
	exit 2
fi

cd $cur_dir

$_ex "cp client.conf $1.ovpn"
$_ex "echo \"<cert>\" >> $1.ovpn"
$_ex "cat $EASYRSA_KEYBASE/pki/issued/$1.crt >> $1.ovpn"
$_ex "echo -e \"</cert>\n<key>\" >> $1.ovpn"
$_ex "cat $EASYRSA_KEYBASE/pki/private/$1.key >> $1.ovpn"
$_ex "echo \"</key>\" >> $1.ovpn"

read -p "Your client is windows?[Y/n] " windows
if check_opposite_answer $windows; then
	$_ex "sed -i.bak \"s/;user nobody/user nobody/g\" $1.ovpn"
	$_ex "sed -i.bak \"s/;group nobody/group nobody/g\" $1.ovpn"
fi

$_ex "rm $1.ovpn.bak"
