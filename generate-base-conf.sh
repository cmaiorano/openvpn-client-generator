#! /usr/bin/env sh

set -e

. base-functions.sh
config_file="config.env"
platform=$(uname)

cleanup(){
	$_ex 'rm client.conf client.conf.bak config.sh 2> /dev/null'
}

if [ "$platform" = 'Linux' ]; then
	$_ex 'cp /usr/share/doc/openvpn/examples/sample-config-files/client.conf .'
elif [ "$platform" = 'FreeBSD' ]; then
	$_ex 'cp /usr/local/share/examples/openvpn/sample-config-files/client.conf .'
fi

read -p "Your server uses tun?[Y/n] " tun
if check_opposite_answer $tun; then
	$_ex 'sed -i.bak "s/;dev tap/dev tap/g" client.conf'
	$_ex 'sed -i.bak "s/dev tun/;dev tun/g" client.conf'
fi

read -p "Your server uses udp?[Y/n] " udp
if check_opposite_answer $tun; then
	$_ex 'sed -i.bak "s/;proto tcp/proto tcp/g" client.conf'
	$_ex 'sed -i.bak "s/proto udp/;proto udp/g" client.conf'
fi

read -p "Which port uses your server? " server_port
read -p "Which is your remote address? " server_remote

$_ex "sed -i.bak \"s/remote my-server-1 1194/remote $server_remote $server_port/g\" client.conf"
$_ex 'sed -i.bak "s/ca ca.crt/#ca ca.crt/g" client.conf'
$_ex 'sed -i.bak "s/cert client.crt/#cert client.crt/g" client.conf'
$_ex 'sed -i.bak "s/key client.key/#key client.key/g" client.conf'
$_ex 'sed -i.bak "s/tls-auth ta.key 1/key-direction 1/g" client.conf'

read -p "Insert the location of ta.key file included file name: " ta_loc
$_ex 'echo "<tls-auth>" >> client.conf'
$_ex "cat $ta_loc >> client.conf"
$_ex 'echo -e "</tls-auth>\n<ca>" >> client.conf'

read -p "Insert the location of ca certificate included file name: " ca_loc
$_ex "cat $ca_loc >> client.conf"
$_ex 'echo "</ca>" >> client.conf'

$_ex 'rm client.conf.bak'

$_ex "touch $config_file"
$_ex "echo \". base-functions.sh\" >> $config_file"

EASYRSA_EXEC="$(command -v easyrsa)"

if ! check_binary "easyrsa"; then
	read -p "Seems that easyrsa is not in your path, please insert the location of your easyrsa command: " easyrsa_read
	EASYRSA_EXEC=$easyrsa_read
fi

$_ex "echo \"export EASYRSA_EXEC=$EASYRSA_EXEC\" >> $config_file"

vars_file="/usr/local/share/easy-rsa/vars"
read -p "Are you using different vars file?[y/N] " different_vars
if check_answer $different_vars; then
	read -p "Please insert the path to the vars file [EXCLUDING THE VARS FILE ITSELF]: " vars_path
	vars_file=$vars_path
fi

$_ex "echo \"export EASYRSA=$vars_path\" >> $config_file"

read -p "Where is your pki base folder (the folder which contains the pki folder)? " pki_path
$_ex set EASYRSA_KEYBASE $pki_path
$_ex "echo \"export EASYRSA_KEYBASE=$pki_path\" >> $config_file"
$_ex "echo \"export EASYRSA_PKI=$EASYRSA_KEYBASE/pki\" >> $config_file"
