#! /usr/bin/env sh

_usage () {
	echo "Usage: $0 <client-name>"
    exit 1
}

check_binary () {
	echo -n " * Checking for '$1' ... "
	if command -v $1 >/dev/null 2>&1; then
		echo "OK"
		return 0
	else
		echo >&2 "FAILED"
		return 1
	fi
}

trap_ctrlc (){
    # perform cleanup here
    echo "Ctrl-C caught...performing clean up"
 
	cleanup

    exit 2
}

trap "trap_ctrlc" 2

check_opposite_answer(){
	if [ "${1}" = "n" -o "${1}" = "N" -o "${1}" = "No" ]; then
		return 0
	fi
	return 1
}

check_answer(){
	if [ "${1}" = "y" -o "${1}" = "Y" -o "${1}" = "Yes" ]; then
		return 0
	fi
	return 1
}

_ex="sudo -E sh -c"

if [ $(id -u) = 0 ]; then
	_ex="sh -c"
fi

